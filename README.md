# Push_Swap

### Description
This project involves sorting data on a stack, with a limited set of instructions, and the smallest number of moves. To make this happen, I had to manipulate various sorting algorithms and choose the most appropriate solution(s) for optimized data sorting. 

***
### Makefile
Makefile a the root allowed you to compile the libft, the chercker and the push_swap.

So you can run:
```bash
make libft
make checker
make push_swap
```
Or simply:
```bash
make
```

***
### What's push_swap?
This project involves sorting data on a stack, with a limited set of instructions,
and the smallest number of moves. To make this happen, you will have to manipulate
various sorting algorithms and choose the most appropriate solution(s) for
optimized data sorting.
